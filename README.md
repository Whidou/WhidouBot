WhidouBot
=========

Source code for Wikidata bot WhidouBot.
User page: https://www.wikidata.org/wiki/User:WhidouBot .

Adds missing descriptions to music albums.

Dependencies
------------

* [Python 3](https://www.python.org/)
* [Pywikibot](https://gerrit.wikimedia.org/r/#/admin/projects/pywikibot)
