#!/usr/bin/python3

# Copyright (C) 2017 Whidou <whidou@openmailbox.org>. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
# FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import pywikibot
from pywikibot.data import sparql
import time

QUERY = """
SELECT DISTINCT ?album ?artist WHERE
{{
  {{
    SELECT ?album ( COUNT(DISTINCT ?artist ) as ?nArtists ) WHERE
    {{
        ?album wdt:P31 wd:Q482994 .
        ?album wdt:P175 ?artist .

        MINUS
        {{
            ?album schema:description ?description .
            FILTER( LANG( ?description ) = "{0}" ) .
        }}
    }}
    GROUP BY ?album
    HAVING(?nArtists = 1)
  }}

  ?album wdt:P175 ?artist .
  ?artist rdfs:label ?artistLabel .
  FILTER( LANG( ?artistLabel ) = "{0}" ) .
}}
LIMIT {1}
"""

DESCRIPTIONS = {
    "en": "album by {}",
    "eo": "albumo de {}",
    "es": "álbum de {}",
    "fr": "album de {}",
    "ja": "{}のアルバム",
    "ro": "album de {}",
    "sq": "album nga {}"
}

def main():
    # Get language from arguments
    if len(sys.argv) < 2:
        language = "fr"
    else:
        language = sys.argv[1]

    # Check language
    if language not in DESCRIPTIONS:
        print("Error: unknown language code:", language, file=sys.stderr)
        print("Supported languages:", ", ".join(DESCRIPTIONS), file=sys.stderr)
        return 1

    # Get query limit from arguments
    if len(sys.argv) < 3:
        limit = 100
    else:
        try:
            limit = int(sys.argv[2], 10)
        except ValueError:
            print("Error: invalid limit", file=sys.stderr)
            return 2

    # Configure pywikibot
    site = pywikibot.Site("wikidata", "wikidata")
    repo = site.data_repository()

    # Generate query
    formattedQuery = QUERY.format(language, limit)

    # Send query
    query_object = sparql.SparqlQuery()
    data = query_object.select(formattedQuery)

    # Check result
    if not data:
        print("No matching item found")
        return 0

    for result in data:
        # Get QID from URL
        albumQid = result["album"].split("/")[-1]
        artistQid = result["artist"].split("/")[-1]

        # Get artist labels
        artist = pywikibot.ItemPage(repo, artistQid)
        try:
            artistLabels = artist.get()["labels"]
        except pywikibot.Error as error:
            print(error, file=sys.stderr)
            continue
        except KeyboardInterrupt:
            break

        # Get current album descriptions
        album = pywikibot.ItemPage(repo, albumQid)
        try:
            albumDescriptions = album.get()["descriptions"]
        except pywikibot.Error as error:
            print(error, file=sys.stderr)
            continue
        except KeyboardInterrupt:
            break

        # Generate descriptions
        descriptions = {}
        for lang in DESCRIPTIONS:
            # Check that there is no existing album description for this
            # language and that an artist label is available
            if lang in albumDescriptions or lang not in artistLabels:
                continue
            descriptions[lang] = DESCRIPTIONS[lang].format(artistLabels[lang])
        if len(descriptions) == 0:
            print("WARNING: No description to add to", albumQid,
                  file=sys.stderr)
            continue

        print(albumQid, artistQid, descriptions)

        # Generate edit summary
        summary = "Add {} description.".format("/".join(descriptions.keys()))

        try:
            album.editDescriptions(descriptions, summary=summary)
        except pywikibot.exceptions.OtherPageSaveError as error:
            # In case of description conflict, notify the operator
            print(error, file=sys.stderr)
        except KeyboardInterrupt:
            break

if __name__ == "__main__":
    exitCode = main()
    exit(exitCode)
